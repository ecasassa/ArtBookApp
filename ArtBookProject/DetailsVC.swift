//
//  DetailsVC.swift
//  ArtBookProject
//
//  Created by Etienne Casassa on 2/15/23.
//

import UIKit
import CoreData

class DetailsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var chosenPainting = ""
    var chosenPaintingId: UUID?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var artistText: UITextField!
    @IBOutlet weak var yearText: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if chosenPainting != "" {
            
            // hide save button if chosenPainting has a string in it.
            saveButton.isHidden = false
            
            // loading the data if they picked and existing entity

            // fetching Core Data
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let context = appDelegate?.persistentContainer.viewContext

            // get the entity
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Paintings")
            
            // get the UUID
            let idString = chosenPaintingId?.uuidString
            
            // find ID being equal to idString
            fetchRequest.predicate = NSPredicate(format: "id = %@", idString!)
            
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                let results = try context?.fetch(fetchRequest)
                
                if results?.count ?? 0 > 0 {
                    for result in results as! [NSManagedObject] {
                        if let name = result.value(forKey: "name") as? String {
                            nameText.text = name
                        }
                        if let artist = result.value(forKey: "artist") as? String {
                            artistText.text = artist
                        }
                        if let year = result.value(forKey: "year") as? Int {
                            yearText.text = String(year)
                        }
                        if let imageData = result.value(forKey: "image") as? Data {
                            let image = UIImage(data: imageData)
                            imageView.image = image
                        }
                    }
                }
                
            } catch {
                print("error finding results")
            }
            
        } else {
            
            saveButton.isHidden = false
            saveButton.isEnabled = false
            
            // these should also be empty by default, but just in case...
            nameText.text = ""
            artistText.text = ""
            yearText.text = ""
        }

        // dismiss keyboard
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(gestureRecognizer)

        // make the image clickable
        imageView.isUserInteractionEnabled = true
        let imageTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectImage))
        imageView.addGestureRecognizer(imageTapRecognizer)
        
    }
    
    // make it a button that searches the PhotoLibrary
    @objc func selectImage() {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true)
        
    }
    
    // place image in imageView and dismiss
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageView.image = info[.originalImage] as? UIImage
        saveButton.isEnabled = true
        self.dismiss(animated: true)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        
        // where to save selections
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let newPainting = NSEntityDescription.insertNewObject(forEntityName: "Paintings", into: context)
        
        // set attributes for the typed texts
        
        newPainting.setValue(nameText.text ?? "", forKey: "name")
        newPainting.setValue(artistText.text ?? "", forKey: "artist")
        
        if let year = Int(yearText.text ?? "") {
            newPainting.setValue(year, forKey: "year")
        }
        
        // create new id for the entity
        newPainting.setValue(UUID(), forKey: "id")
        
        // make the quality of the image lower so it doesnt take too much space
        let data = imageView.image?.jpegData(compressionQuality: 0.5)
        // set image attribute
        newPainting.setValue(data, forKey: "image")
        
        
        // actually save all my attributes
        do {
            try context.save()
            print("success saving new selection")
        } catch {
            print("error saving selection")
        }

        // post a notification so everyone in the app can see it
        NotificationCenter.default.post(name: NSNotification.Name("newData"), object: nil)
        // take me back to the tableView VC.
        self.navigationController?.popViewController(animated: true)
        
    }

}
