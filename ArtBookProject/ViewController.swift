//
//  ViewController.swift
//  ArtBookProject
//
//  Created by Etienne Casassa on 2/15/23.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var nameArray = [String]()
    var idArray = [UUID]()
    var selectedPainting = ""
    var selectedPaintingId: UUID?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // adding the button on the top right of the bar in the tableViewVC
        navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addButtonClicked))
        
        getData()
        
    }
    
    // when hitting Save, this takes you back to the tableView
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(getData), name: NSNotification.Name("newData"), object: nil)
    }
    
    @objc func getData() {
        
        // delete values so you don't re-load everything again when you save something
        nameArray.removeAll(keepingCapacity: false)
        
        // get data from coreData
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = appDelegate?.persistentContainer.viewContext
        
        // get the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Paintings")
        // false will make things quicker?
        fetchRequest.returnsObjectsAsFaults = false
        
        // fetch name and ID and reload the tableView
        do {
            let results = try context?.fetch(fetchRequest)
            
            if results?.count ?? 0 > 0 {
                for result in results as! [NSManagedObject] {
                    if let name = result.value(forKey: "name") as? String {
                        self.nameArray.append(name)
                    }
                    if let id = result.value(forKey: "id") as? UUID {
                        self.idArray.append(id)
                    }
                    
                    tableView.reloadData()
                }
            }
        } catch {
            print ("couldn't fetch data")
        }
    }
    
    // programatically added the button
    @objc func addButtonClicked() {
        // make sure the that selectedPainting is empty before sending to the DetailsVC
        selectedPainting = ""
        performSegue(withIdentifier: "toDetailsVC", sender: nil)
    }
    
    // match info from once VC to the other
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetailsVC" {
            let destinationVC = segue.destination as! DetailsVC
            destinationVC.chosenPainting = selectedPainting
            destinationVC.chosenPaintingId = selectedPaintingId
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = nameArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // send the info over to the other vc
        selectedPainting = nameArray[indexPath.row]
        selectedPaintingId = idArray[indexPath.row]
        performSegue(withIdentifier: "toDetailsVC", sender: nil)
    }
        
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            // need to get the element from the core data, then delete it, and then save
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let context = appDelegate?.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Paintings")
            
            // get the row selected
            let idString = idArray[indexPath.row].uuidString
            
            fetchRequest.predicate = NSPredicate(format: "id = %@", idString)
            
            fetchRequest.returnsObjectsAsFaults = false
            
            do {
                let results = try context?.fetch(fetchRequest)
                
                if results?.count ?? 0 > 0 {
                    for result in results as! [NSManagedObject] {
                        
                        if let id = result.value(forKey: "id") as? UUID {
                            
                            // comparing IDs to make sure it's the same
                            if id == idArray[indexPath.row] {
                                context?.delete(result)
                                nameArray.remove(at: indexPath.row)
                                idArray.remove(at: indexPath.row)
                                
                                // reload the list
                                self.tableView.reloadData()
                                
                                // save removing it
                                do {
                                    try context?.save()
                                } catch {
                                    print("error removing item")
                                }
                                
                                // break out of the for loop to be on the safe side. Not necessary, but good. Especially when we're using something different than the unique UUID
                                break
                            }
                        }
                    }
                }
            } catch {
                print("error fetching to delete")
            }
            
            
        }
    }
    
}

